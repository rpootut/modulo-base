﻿using System;
using ModuloBase.Clases;

namespace ModuloBase
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona persona1 = new Persona("AdonisJS");
            //persona1.Nombre = "AdonisJS";
            persona1.SueldoMensual = 25000;

            Console.WriteLine($"Su nombre es: {persona1.Nombre}");
            Console.WriteLine($"Su sueldo es: {persona1.SueldoMensual}");
            Console.WriteLine($"Su sueldo anual es: {persona1.SueldoAnual}");

            persona1.Telefonos.Add("9981114177");
            persona1.Telefonos.Add("9981918343");
            persona1.Telefonos.Add("9982926093");

            foreach (string telefono in persona1.Telefonos)
            {
                Console.WriteLine("El telefono es: " + telefono);
            }

            persona1.Hablar();
            persona1.Hablar("Lopez Cano");
            persona1.Hablar("Fierro", 10);

            /*CambiarNombre(persona1);

            Console.WriteLine($"Su nuevo nombre es: {persona1.Nombre}");

            int numero = 1;

            Console.WriteLine("El numero es: " + numero);

            CambiarNumero(ref numero);

            Console.WriteLine("El nuevo numero es: " + numero);
            */
        }

        static void CambiarNombre(Persona persona)
        {
            persona.Nombre = ":)";
        }

        static void CambiarNumero(ref int numero)
        {
            numero = 20;
        }
    }
}
