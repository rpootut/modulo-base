﻿using System;
using System.Collections.Generic;

namespace ModuloBase.Clases
{
    public class Persona
    {
        public Persona()
        {
            Console.WriteLine("Objeto Persona :3");
            Telefonos = new List<string>();
        }

        public Persona(string nombre):this()
        {
            Nombre = nombre;
        }

        public List<string> Telefonos { get; set; }

        public string Nombre { get; set; }

        public double SueldoMensual { get; set; }

        public double SueldoAnual {
            get
            {
                return SueldoMensual * 12;
            }
        }

        public void Hablar()
        {
            Console.WriteLine($"Hola, soy {Nombre}");
        }

        public void Hablar(string nombre)
        {
            Console.WriteLine($"Hola {nombre}, soy {Nombre}");
        }

        public void Hablar(string nombre, int numero)
        {
            Console.WriteLine($"Hola {nombre}, soy {Nombre} el numero es {numero}");
        }

        // forma larga
        // private string nombre;

        /*public string Nombre
        {
            get
            {
                Console.WriteLine("Se obtuvo el nombre de la persona");
                return nombre;
            }
            set
            {
                Console.WriteLine("Se cambio el nombre de la persona");
                nombre = value;
            }
        }*/
    }
}
